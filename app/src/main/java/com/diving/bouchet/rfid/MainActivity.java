package com.diving.bouchet.rfid;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity{

    private TextView logview;
    private TextView status;

    private BtInterface bt = null;

    private long lastTime = 0;

    /**
     * Evènement qui receptionne les données
     */
    final Handler handler = new Handler() {
        public void handleMessage(Message msg) {

            String data = msg.getData().getString("receivedData");

            long t = System.currentTimeMillis();
            if(t-lastTime > 500) {// Pour éviter que les messages soit coupés
                logview.append("\n");
                lastTime = System.currentTimeMillis();
            }
            if(data.equals("#"))
            {
                logview.append(data);
                logview.setText("");
            }
            else
                logview.append(data);
        }
    };


    /**
     * Evènement qui gère la connexion
     */
    final Handler handlerStatus = new Handler() {
        public void handleMessage(Message msg) {
            int co = msg.arg1;
            if(co == 1) {
                status.setText(R.string.Connected);
            } else if(co == 2) {
                status.setText(R.string.Disconnected);
            }
        }
    };

    /**
     *  Called when the activity is first created.
     *  */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        logview = (TextView) findViewById(R.id.logview);
        status = (TextView)findViewById(R.id.status);

        if(controleBluetooth())
        {
            bt = new BtInterface(handlerStatus, handler);

            // Connexion
            this.status.setText("Connexion ....");
            bt.connect();
        }

        else
        {
            Intent i = new Intent(this, NotCompatible.class);
            startActivity(i);
        }

    }

    /**
     * Controle si l'appareil a bien le bluetooth
     * @return true si ok
     */
    private boolean controleBluetooth()
    {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            return false;
        }
        return true;
    }
}